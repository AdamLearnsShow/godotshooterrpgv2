extends Node

class_name Gameplay

signal game_over()

var player:Node = null
var gameplay_ui:GameplayUI
var can_spawn_enemies:bool = false

func init(character_class:int, gameplay_ui_node:GameplayUI) -> void:
	gameplay_ui = gameplay_ui_node
	
	create_player(character_class)
	
func create_player(character_class:int) -> void:
	var character_class_info:Dictionary = Constants.character_class_info[character_class]
	var scene_path:String = character_class_info.scene_path
	var scene:PackedScene = load(scene_path)
	player = scene.instance()
	player.init(character_class)
	add_child(player)
	gameplay_ui.set_all_player_info(player)
	
	connect_player_signals()
	
func connect_player_signals() -> void:
	var _ignored
	_ignored = player.connect("rotated", self, "_on_rotated")
	_ignored = player.connect("fired", self, "_on_fire_projectile")
	
	_ignored = player.stats.connect("life_changed", gameplay_ui, "_on_player_life_changed")
	_ignored = player.stats.connect("energy_changed", gameplay_ui, "_on_player_energy_changed")
	_ignored = player.stats.connect("experience_changed", gameplay_ui, "_on_player_experience_changed")
	_ignored = player.stats.connect("leveled_up", self, "_on_player_leveled_up")
	_ignored = player.stats.connect("life_changed", self, "_on_player_life_changed")
	
func _on_player_life_changed(life:float) -> void:
	if life <= 0:
		emit_signal("game_over")

func _on_player_leveled_up(_level:int) -> void:
	gameplay_ui.player_leveled_up(player)

func _on_fire_projectile(projectile_params:ProjectileParams) -> void:
	fire_projectile(projectile_params)

func _ready() -> void:
	# Categorize all obstacles as being billboarded, that way we can iterate 
	# over every member in the Constants.GroupNameBillboarded rather than have
	# to keep track of individual node paths.
	var nodes:Array = $WorldMap/Obstacles.get_children()
	for child in nodes:
		child.add_to_group(Constants.GroupNameBillboarded)
	
	# Connect the "spawn_enemy" signal to our function.
	nodes = get_tree().get_nodes_in_group(Constants.GroupNameMonsterZones)
	for child in nodes:
		var _ignored = child.connect("spawn_enemy", self, "_on_spawn_enemy")
	
	make_start_game_timer()

func make_start_game_timer() -> void:
	var timer:Timer = Timer.new()
	add_child(timer)
	timer.wait_time = 2

	timer.start()
	yield(timer, "timeout")
	can_spawn_enemies = true

# This is called when the player rotates the camera.
func _on_rotated(rotation:float) -> void:
	var nodes:Array = get_tree().get_nodes_in_group(Constants.GroupNameBillboarded)
	for child in nodes:
		child.rotation = rotation

func fire_projectile(projectile_params:ProjectileParams) -> void:
	projectile_params.billboard_rotation = player.rotation
	var projectile_scene:PackedScene = load(projectile_params.scene_path)
	var projectile:KinematicBody2D = projectile_scene.instance()
	var script_path:String = projectile_params.script_path
	var projectile_script:GDScript = load(script_path)
	
	projectile.set_script(projectile_script)
	projectile.init(projectile_params)
	add_child(projectile)
	
func _on_spawn_enemy(enemy_type:int, level:int, global_position:Vector2) -> void:
	if !can_spawn_enemies:
		return
	var enemy_info:Dictionary = Constants.enemy_info[enemy_type]
	var enemy_scene:PackedScene = load(enemy_info.scene_path)

	var enemy:Node2D = enemy_scene.instance()
	var enemy_script:GDScript = preload("res://scripts/EnemyController.gd")
	enemy.position = global_position
	enemy.set_script(enemy_script)
	enemy.init(player.rotation, enemy_info, level)
	add_child(enemy)
	
	var _ignored = enemy.connect("fired", self, "_on_fire_projectile")
	_ignored = enemy.connect("died", self, "_on_enemy_died")

func _on_enemy_died(experience_granted:int) -> void:
	Util.add_sound_to_node_by_sound_file("res://assets/sounds/enemy_death.wav", self)
	player.stats.gain_experience(experience_granted)
