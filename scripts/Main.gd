extends Node

# Remove and free the current scene from the tree. We always assume that the
# first child is the root of the scene.
func remove_current_scene() -> void:	
	var current_scene:Node = get_children()[0]
	remove_child(current_scene)
	current_scene.queue_free()

func setup_character_select_scene() -> void:
	var CharacterSelectScene:PackedScene = preload("res://scenes/CharacterSelect.tscn")
	var character_select_scene:Node = CharacterSelectScene.instance()

	add_child(character_select_scene)

	var _ignored = character_select_scene.connect("selected_character", self, "_on_character_selected")

func _on_character_selected(character_class:int) -> void:
	remove_current_scene()
	
	var GameplayAndUIScene:PackedScene = preload("res://scenes/GameplayAndUI.tscn")
	var gameplay_and_ui_scene:Node = GameplayAndUIScene.instance()
	add_child(gameplay_and_ui_scene)
	gameplay_and_ui_scene.init(character_class)
	
	var _ignored = gameplay_and_ui_scene.gameplay.connect("game_over", self, "_on_game_over")

func _on_game_over():
	Util.add_sound_to_node_by_sound_file("res://assets/sounds/player_death.wav", self)
	remove_current_scene()
	setup_character_select_scene()
	
func _ready() -> void:
	setup_character_select_scene()

func _unhandled_input(_event) -> void:
	if Input.is_action_just_pressed("toggle_sounds"):
		toggle_bus_mute("Sounds")
	if Input.is_action_just_pressed("toggle_music"):
		toggle_bus_mute("Music")

func toggle_bus_mute(bus_name:String) -> void:
	var bus_index:int = AudioServer.get_bus_index(bus_name)
	var muted:bool = AudioServer.is_bus_mute(bus_index)
	AudioServer.set_bus_mute(bus_index, !muted)
	

