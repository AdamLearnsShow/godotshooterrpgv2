extends Node

class_name Stats

signal life_changed(life)
signal energy_changed(energy)
signal experience_changed(experience)
signal leveled_up(level)

var level:int = 1
var experience:int = 0
var life:float = 100
var max_life:float = 100
var energy:float = 100
var max_energy:float = 100
var health_regen:float = 0
var energy_regen:float = 0
var attack:float = 10
var defense:float = 10
var movement_speed:float = 300
var fire_reset:float = 1

var experience_needed_to_level:int

# Any parameters in here will get added to this particular Stats instance upon
# leveling up. Stats in its entirety could have been refactored to allow
# stat_growth to be a proper Stats object instead of a Dictionary, but I forego
# type safety for ease of showing off the basics of how stats work.
var stat_growth:Dictionary

func _init(level_to_set:int, stats_dictionary:Dictionary, stat_growth_dictionary:Dictionary):
	stat_growth = stat_growth_dictionary

	max_life = stats_dictionary.max_life
	if "max_energy" in stats_dictionary:
		max_energy = stats_dictionary.max_energy
	if "health_regen" in stats_dictionary: health_regen = stats_dictionary.health_regen
	if "energy_regen" in stats_dictionary: energy_regen = stats_dictionary.energy_regen
	if "fire_reset" in stats_dictionary: fire_reset = stats_dictionary.fire_reset

	# Players set experience to know when to level up.
	# Enemies set experience so that we know how much to grant to the player.
	if "experience" in stats_dictionary: experience = stats_dictionary.experience
	attack = stats_dictionary.attack
	defense = stats_dictionary.defense
	movement_speed = stats_dictionary.movement_speed
	
	set_level(1)
	grow_to_level(level_to_set)
	restore_to_full()

# We pass in a node because apparently Godot doesn't allow timers to work purely
# through code, otherwise Stats wouldn't need to exist in the scene tree at all.
#
# I suspect that this is a bug with Godot.
func set_up_regen_timer(node_to_attach_to:Node) -> void:
	var timer:Timer = Timer.new()
	node_to_attach_to.add_child(timer)
	timer.wait_time = 1

	while true:
		apply_regen()
		timer.start()
		yield(timer, "timeout")

func restore_to_full() -> void:
	set_life(max_life)
	set_energy(max_energy)

func set_level(new_level:int) -> void:
	level = new_level
	
	# Once the player is at the highest level for which we specified the
	# required experience, we'll just keep using that same value over and over.
	var highest_level_specified:int = Constants.experience_needed_to_level.size() - 1
	var level_index:int = int(min(highest_level_specified, level - 1))
	experience_needed_to_level = Constants.experience_needed_to_level[level_index]

func grow_single_level() -> void:
	set_level(level + 1)
	if "max_life" in stat_growth: max_life += stat_growth.max_life
	if "max_energy" in stat_growth: max_energy += stat_growth.max_energy
	if "health_regen" in stat_growth: health_regen += stat_growth.health_regen
	if "energy_regen" in stat_growth: energy_regen += stat_growth.energy_regen
	if "attack" in stat_growth: attack += stat_growth.attack
	if "defense" in stat_growth: defense += stat_growth.defense
	if "movement_speed" in stat_growth: movement_speed += stat_growth.movement_speed
	if "fire_reset" in stat_growth: fire_reset = max(0.01, fire_reset - stat_growth.fire_reset)

func grow_to_level(target_level:int) -> void:
	var num_levels_to_grow:int = target_level - level
	if num_levels_to_grow <= 0:
		return

	for _i in range(num_levels_to_grow):
		grow_single_level()

func set_max_life(new_max_life:int) -> void:
	max_life = new_max_life
	set_life(new_max_life)

func gain_experience(experience_to_gain:int) -> bool:
	experience += experience_to_gain
	var leveled_at_least_once:bool = false
	while true:
		if experience < experience_needed_to_level:
			break

		experience -= experience_needed_to_level
		leveled_at_least_once = true
		grow_single_level()

	if leveled_at_least_once:
		restore_to_full()

	emit_signal("experience_changed", experience)
	
	if leveled_at_least_once:
		emit_signal("leveled_up", level)
		
	return leveled_at_least_once

func restore_life(amount:float) -> void:
	set_life(life + amount)

func restore_energy(amount:float) -> void:
	set_energy(energy + amount)

func apply_regen() -> void:
	restore_life(health_regen)
	restore_energy(energy_regen)
	
func set_life(new_life:float) -> void:
	var old_life:float = life
	
	life = min(max_life, new_life)
	
	if old_life != new_life:
		emit_signal("life_changed", life)
	
func set_energy(new_energy:float) -> void:
	var old_energy:float = energy
	
	energy = min(max_energy, new_energy)
	
	if old_energy != new_energy:
		emit_signal("energy_changed", energy)

# This is just a very simple damage calculation. It does mean that if you get a
# high enough defense statistic that you'll be immune to damage.
static func get_damage_from_attack_and_defense(atk:float, def:float) -> float:
	return max(0, atk - def)

func has_enough_energy(energy_cost:float) -> bool:
	return energy >= energy_cost

func remove_energy(energy_cost:float) -> void:
	set_energy(energy - energy_cost)

func remove_life(amount:float) -> void:
	set_life(life - amount)
