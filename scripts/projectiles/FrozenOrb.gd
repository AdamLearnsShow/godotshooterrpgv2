extends Projectile

class_name FrozenOrbProjectile

# This represents at what angle the next icebolt will shoot
var turret_rotation:float

# We'll shrink this projectile as it sheds icebolts
var scale_override:float = 2.5

func init(projectile_params:ProjectileParams) -> void:
	.init(projectile_params)

	turret_rotation = 0
	
	# Increase TTL based on the firer's level. Ideally, this would be done from
	# the ability itself.
	params.ttl += params.firer.stats.level * 0.2

func _ready() -> void:
	# This should only collide with walls, not with entities
	collision_layer = 0
	collision_mask = Util.convert_enum_to_bitmask([Constants.PhysicsMasks.WORLD_WALLS])

	set_up_fire_timer()

func set_up_fire_timer() -> void:
	var timer:Timer = Timer.new()
	add_child(timer)
	timer.wait_time = 0.03
	var play_sound_counter = 0

	while true:
		play_sound_counter -= 1
		if play_sound_counter <= 0:
			Util.add_sound_to_node_by_sound_file("res://assets/sounds/frozen_orb.wav", get_parent())
			play_sound_counter = 3
		fire_projectile()
		timer.start()
		yield(timer, "timeout")

func fire_projectile() -> void:
	var projectile_type:int = Constants.ProjectileTypes.ICEBOLT
	var vector:Vector2 = Vector2.UP.rotated(turret_rotation).normalized()
	turret_rotation += PI / 8

	var projectile_params:ProjectileParams = ProjectileParams.new({
		"firer": params.firer,
		"override_start_position": position,
		"projectile_type": projectile_type,
		"target": vector,
		"treat_target_as_position": false,
		"damage": params.damage,
	})
	params.firer.fire_projectile(projectile_params)

func _physics_process(delta:float) -> void:
	._physics_process(delta)

	rotation += PI * delta
	scale = Vector2(scale_override, scale_override)
	scale_override = max(0.1, scale_override - delta)
