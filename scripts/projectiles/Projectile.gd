extends KinematicBody2D

class_name Projectile

var params:ProjectileParams
var velocity_direction:Vector2

# This is so that we don't count collisions multiple times
var is_dead:bool = false

func init(projectile_params:ProjectileParams) -> void:
	params = projectile_params
	
	if params.override_start_position == null:
		position = params.firer.position
	else:
		position = params.override_start_position
	
	if params.treat_target_as_position:
		velocity_direction = position.direction_to(params.target)
	else:
		velocity_direction = params.target
		
	if params.is_billboarded:
		rotation = params.billboard_rotation
		add_to_group(Constants.GroupNameBillboarded)
	else:
		rotation = Vector2.UP.angle_to(velocity_direction)
		
	scale = Vector2(params.scale, params.scale)
	add_to_group(Constants.GroupNameProjectiles)
	
	var is_player:bool = params.firer.is_in_group(Constants.GroupNamePlayers)
	var collision_layer:int = Constants.PhysicsMasks.PLAYER_PROJECTILE if is_player else Constants.PhysicsMasks.ENEMY_PROJECTILE
	var collision_mask:int = Constants.PhysicsMasks.ENEMY if is_player else Constants.PhysicsMasks.PLAYER
	self.collision_layer = Util.convert_enum_to_bitmask([collision_layer])
	self.collision_mask = Util.convert_enum_to_bitmask([Constants.PhysicsMasks.WORLD_WALLS, collision_mask])

func get_damage() -> float:
	return params.damage

func _physics_process(delta:float) -> void:
	rotation += delta * params.rotation_speed
		
	var _ignored = move_and_slide(velocity_direction * params.speed)
	
	if get_slide_count() > 0:
		# Note that if a projectile is colliding with multiple bodies (e.g. the
		# world AND the player), then by only getting the first collision, we
		# may not handle the collision with the player. This is a relatively
		# minor bug.
		var collision:KinematicCollision2D = get_slide_collision(0)
		var body:Object = collision.collider
		collided_with_body(body)

	params.ttl -= delta
	if params.ttl <= 0:
		queue_free()
	
	# Fade projectiles out toward the end of their lifespans
	if params.ttl < 0.1:
		modulate[3] = (params.ttl / 0.1)

# This is called when this collides with a body or when a body collides with
# this. Godot considers those two events to be different, but from a player's
# perspective, they both represent this projectile hitting something.
func collided_with_body(body:Node) -> void:
	if is_dead:
		return
		
	is_dead = true
	
	if body.has_method("projectile_hit"):
		body.projectile_hit(self)
		queue_free()
