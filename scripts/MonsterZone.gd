extends Node2D

export var min_level:int = 1
export var max_level:int = 2
export var min_spawn_group_size:int = 1
export var max_spawn_group_size:int = 5
export(Array, Constants.EnemyNames) var monster_types

signal spawn_enemy(enemy_type, level, world_x, world_y)

# These are the boundaries of our spawn zone as individual coordinates. This
# zone never moves, so we can save off the values at initialization time.
var spawn_min_x:float
var spawn_min_y:float
var spawn_max_x:float
var spawn_max_y:float

func _ready() -> void:
	# Make sure the minimum level isn't lower than 1
	min_level = int(max(1, min_level))
	
	var spawn_zone = $Area2D
	var spawn_zone_collision = $Area2D/CollisionShape2D
	
	# Save off the extents of our spawn zone
	var extents = spawn_zone_collision.shape.extents
	spawn_min_x = spawn_zone_collision.global_position.x - extents.x
	spawn_min_y = spawn_zone_collision.global_position.y - extents.y
	spawn_max_x = spawn_zone_collision.global_position.x + extents.x
	spawn_max_y = spawn_zone_collision.global_position.y + extents.y

	# We'll make the trigger zone be some number of tiles outside of the spawn
	# zone. Ideally, we'd do this based on exactly how far away the camera can
	# see.
	var horizontal_trigger_zone_width:int = 12 * Constants.tilesize;
	var vertical_trigger_zone_width:int = 12 * Constants.tilesize;
	
	var trigger_zone:Area2D = Area2D.new()
	var trigger_zone_collision:CollisionShape2D = CollisionShape2D.new()
	trigger_zone_collision.shape = RectangleShape2D.new()
	trigger_zone.position = spawn_zone.position
	trigger_zone.collision_layer = spawn_zone.collision_layer
	trigger_zone.collision_mask = spawn_zone.collision_mask
	trigger_zone_collision.shape.extents.x = extents.x + horizontal_trigger_zone_width
	trigger_zone_collision.shape.extents.y = extents.y + vertical_trigger_zone_width
	trigger_zone.add_child(trigger_zone_collision)
	add_child(trigger_zone)
	
	add_to_group(Constants.GroupNameMonsterZones)

	# The CONNECT_DEFERRED is needed to avoid a bunch of errors when the event
	# itself is called. This is likely because we'll be adding bodies to the
	# region that may try to trigger calls to the region (even though their
	# collision_layer shouldn't match).
	var _ignored = trigger_zone.connect("body_entered", self, "_on_body_entered", [], CONNECT_DEFERRED)

func _on_body_entered(_body:Node) -> void:
	spawn_enemies()

func spawn_enemies() -> void:
	var num_enemies_to_spawn:int = Util.randint_range(min_spawn_group_size, max_spawn_group_size)
	for _i in range(num_enemies_to_spawn):
		var enemy_type:int = Util.sample_array(monster_types)
		var global_position:Vector2 = Vector2(
			rand_range(spawn_min_x, spawn_max_x),
			rand_range(spawn_min_y, spawn_max_y)
		)
		
		var level = Util.randint_range(min_level, max_level)

		emit_signal("spawn_enemy", enemy_type, level, global_position)
