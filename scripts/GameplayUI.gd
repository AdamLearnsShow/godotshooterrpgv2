extends CanvasLayer

class_name GameplayUI

onready var experience_bar:TextureProgress = $GridContainer/MetersGrid/ExperienceBar
onready var health_bar:TextureProgress = $GridContainer/MetersGrid/HealthBar
onready var energy_bar:TextureProgress = $GridContainer/MetersGrid/EnergyBar
onready var level_label:TextureProgress = $GridContainer/MetersGrid/ExperienceBar/LevelLabel
onready var health_label:TextureProgress = $GridContainer/MetersGrid/HealthBar/HealthLabel
onready var energy_label:TextureProgress = $GridContainer/MetersGrid/EnergyBar/EnergyLabel
onready var attack_label:TextureProgress = $GridContainer/StatsGrid/AttackLabel
onready var defense_label:TextureProgress = $GridContainer/StatsGrid/DefenseLabel
onready var speed_label:TextureProgress = $GridContainer/StatsGrid/SpeedLabel
onready var ability_grid:TextureProgress = $GridContainer/AbilityGrid
var ability_labels:Array

func _ready() -> void:
	ability_labels = ability_grid.get_children()

func set_experience(value:float) -> void:
	experience_bar.value = value

func set_all_stats(stats:Stats) -> void:
	health_bar.max_value = stats.max_life
	health_bar.value = stats.life
	experience_bar.max_value = stats.experience_needed_to_level
	experience_bar.value = stats.experience
	energy_bar.max_value = stats.max_energy
	energy_bar.value = stats.energy
	attack_label.text = "Attack: " + str(round(stats.attack))
	defense_label.text = "Defense: " + str(round(stats.defense))
	speed_label.text = "Movement: " + str(round(stats.movement_speed))
	set_level_label_based_on_level(stats.level)
	update_health_label()
	update_energy_label()

func set_all_abilities(player:PlayerController) -> void:
	var unlocked_active_abilities:Array = player.unlocked_active_abilities

	var ability_label_index:int = 0

	for ability_name in unlocked_active_abilities:
		var ability_script = Constants.ability_info[ability_name].script
		var description = ability_script.get_description(player)
		ability_labels[ability_label_index].text = description
		ability_label_index += 1

func _on_player_life_changed(new_life:float) -> void:
	health_bar.value = new_life
	update_health_label()

func _on_player_energy_changed(new_energy:float) -> void:
	energy_bar.value = new_energy
	update_energy_label()
	
func _on_player_experience_changed(new_experience:int) -> void:
	experience_bar.value = new_experience

func set_all_player_info(player:PlayerController) -> void:
	set_all_stats(player.stats)
	set_all_abilities(player)

func player_leveled_up(player:PlayerController) -> void:
	set_all_player_info(player)

func update_health_label() -> void:
	health_label.text = "HP: %d/%d" % [round(health_bar.value), round(health_bar.max_value)]

func update_energy_label() -> void:
	energy_label.text = "Energy: %d/%d" % [round(energy_bar.value), round(energy_bar.max_value)]

func set_level_label_based_on_level(level:int) -> void:
	level_label.text = "Level " + str(level)
