extends Object

class_name ProjectileParams

var firer:Node2D
var target:Vector2

var script_path:String

# Statically typed values don't allow for null, so instead of allow for
# target_position and target_angle, we instead just have "target" and a boolean
# to discern which case we want.
# https://github.com/godotengine/godot-proposals/issues/162
var treat_target_as_position:bool

# If we forego static typing, we can have a nullable value. This represents a
# Vector2 that can also be null.
var override_start_position

var projectile_type:int
var damage:float
var scale:float

var scene_path:String
var ttl:float
var speed:float
var rotation_speed:float
var is_billboarded:bool
var billboard_rotation:float

# This function is automatically invoked by Godot when you call ".new()" on
# ProjectileParams. We use it here because ProjectileParams is like a C struct
# moreso than it is a script to control a scene.
#
# We take in a Dictionary rather than named parameters because Godot 3.2 doesn't
# support specifying only certain intermediate parameters and not others.
func _init(params:Dictionary) -> void:	
	firer = get_required_value(params, "firer")
	target = get_required_value(params, "target")
	treat_target_as_position = get_required_value(params, "treat_target_as_position")
	projectile_type = get_required_value(params, "projectile_type")
	override_start_position = get_optional_value(params, "override_start_position", null)
	scale = get_optional_value(params, "scale", 1)
	
	damage = get_required_value(params, "damage")
	
	# Load other parameters from Constants
	var projectile_info = Constants.projectile_info[projectile_type]
	scene_path = projectile_info.scene_path
	ttl = projectile_info.ttl
	speed = projectile_info.speed
	rotation_speed = get_optional_value(projectile_info, "rotation_speed", 0.0)
	is_billboarded = get_optional_value(projectile_info, "is_billboarded", false)
	script_path = get_optional_value(projectile_info, "script_path", "res://scripts/projectiles/Projectile.gd")
	
# This indicates that a property is optional.
# params - the dictionary of parameters passed into this class
# property_name - the name of the property you want to fetch.
# default_value - the value to use if the property doesn't exist.
# Returns: the property, if it exists, otherwise default_value
static func get_optional_value(params:Dictionary, property_name:String, default_value = null):
	if params.has(property_name):
		return params.get(property_name)
	
	return default_value

# This indicates that a property is required.
# params - the dictionary of parameters passed into this class
# property_name - the name of the property you want to fetch.
# Returns: the property
static func get_required_value(params:Dictionary, property_name:String):
	assert(params.has(property_name))
	return params.get(property_name)
