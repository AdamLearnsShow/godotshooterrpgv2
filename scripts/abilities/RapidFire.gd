extends Node2D

class_name RapidFireAbility

var RapidFireParticlesScene:PackedScene = preload("res://scenes/particles/RapidFireParticles.tscn")

var player:PlayerController
var timer:Timer = null
const energy_cost:int = 40
var buff_properties:Dictionary

static func get_description(player_with_ability:PlayerController) -> String:
	var ability_params:Dictionary = get_ability_parameters(player_with_ability.stats.level)

	var duration:float = ability_params.duration
	var fire_rate_multiplier:float = ability_params.fire_rate_multiplier
	
	return "Rapid Fire - increases firing rate by %.1fx for %.1f sec. Costs %d energy." % [fire_rate_multiplier, duration, energy_cost]

static func get_ability_parameters(player_level:int) -> Dictionary:
	return {
		"duration": 3.0 + (player_level - 1) * 0.05,
		"fire_rate_multiplier": 5.0 + (player_level - 1) * 0.6
	}
	
func _init(player_node:PlayerController) -> void:
	player = player_node
	
	var ability_params:Dictionary = get_ability_parameters(player.stats.level)

	var duration:float = ability_params.duration
	var fire_rate_multiplier:float = ability_params.fire_rate_multiplier
	
	buff_properties = {
		"name": Constants.BuffNames.RAPID_FIRE,
		"duration": duration,
		"props_to_inject": {
			"reset_multiplier": 1 / fire_rate_multiplier
		}
	}

func use_ability() -> void:
	player.fire_cooldown = 0

	var buff_output:Dictionary = player.add_buff(self)
	timer = buff_output.timer

	if !buff_output.did_extend_existing_buff:
		var particles:Node = RapidFireParticlesScene.instance()
		player.add_child(particles)

	var _ignored = timer.connect("timeout", self, "_on_timeout")

func _on_timeout() -> void:
	var _ignored = player.buffs.erase(buff_properties.name)

	if !player.has_buff(Constants.BuffNames.RAPID_FIRE):
		player.remove_particles("RapidFireParticles")

		queue_free()
