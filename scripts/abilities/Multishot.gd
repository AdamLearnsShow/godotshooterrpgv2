extends Node2D

class_name MultishotAbility

var player:PlayerController
const energy_cost:int = 30

static func get_description(player_with_ability:PlayerController) -> String:
	var ability_params:Dictionary = get_ability_parameters(player_with_ability.stats.level)

	var num_shots:int = ability_params.num_shots
	var num_volleys:int = ability_params.num_volleys

	return "Multishot - fires %d volleys of %d shots each at the target location. Costs %d energy." % [num_shots, num_volleys, energy_cost]

static func get_ability_parameters(player_level:int) -> Dictionary:
	var num_shots:int = 3
	var num_volleys:int = 3
	
	if player_level >= 2: num_shots += 1
	if player_level >= 6: num_shots += 1
	if player_level >= 10: num_shots += 1
	if player_level >= 14: num_shots += 1
	
	if player_level >= 3: num_volleys += 1
	if player_level >= 7: num_volleys += 1
	if player_level >= 11: num_volleys += 1
	if player_level >= 17: num_volleys += 1

	return {
		"num_shots": num_shots,
		"num_volleys": num_volleys
	}

func _init(player_node:PlayerController) -> void:
	player = player_node

func use_ability() -> void:
	var ability_params:Dictionary = get_ability_parameters(player.stats.level)

	var num_shots:int = ability_params.num_shots
	var num_volleys:int = ability_params.num_volleys

	var timer:Timer = Timer.new()
	add_child(timer)
	timer.wait_time = 0.05

	while true:
		use_multishot(num_shots)
		num_volleys -= 1
		if num_volleys <= 0:
			timer.queue_free()
			break
		timer.start()
		yield(timer, "timeout")

	# Now that we fired all volleys, clean up this abiliit
	queue_free()

func use_multishot(num_shots:int) -> void:
	Util.add_sound_to_node_by_sound_file("res://assets/sounds/multishot.wav", player)
	var linear_velocity:Vector2 = player.position.direction_to(player.get_mouse_position())

	# This arc represents 1/12th of a full circle.
	var spray_angle:float = PI / 6
	
	# We are going to shoot projectiles from -spray_angle/2 to spray_angle/2,
	# that way we cover an even amount of space on both sides of the target.
	var cur_angle:float = -spray_angle / 2

	for _i in range(num_shots):
		var projectile_params:ProjectileParams = ProjectileParams.new({
			"firer": player,
			"projectile_type": player.character_class_info.projectile_type,
			"damage": player.stats.attack,
			"target": linear_velocity.rotated(cur_angle),
			"treat_target_as_position": false,
		})
		player.fire_projectile(projectile_params)
		cur_angle += spray_angle / (num_shots-1)
