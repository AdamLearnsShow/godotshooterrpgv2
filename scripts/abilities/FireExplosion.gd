extends Node2D

class_name FireExplosionAbility

var player:PlayerController
const energy_cost:int = 35

static func get_description(player_with_ability:PlayerController) -> String:
	var num_fireballs:int = get_num_fireballs(player_with_ability.stats.level)
	return "Fire Explosion - creates %d fireballs at the target location. Costs %d energy." % [num_fireballs, energy_cost]

static func get_num_fireballs(player_level:int) -> int:
	return player_level + 15

func _init(player_node:PlayerController) -> void:
	self.player = player_node

func use_ability() -> void:
	Util.add_sound_to_node_by_sound_file("res://assets/sounds/fire_explosion.wav", player)
	var projectile_type:int = Constants.ProjectileTypes.FIREBALL
	var num_fireballs:int = get_num_fireballs(player.stats.level)
	
	# We're taking a full circle (PI * 2 radians or 360°) and dividing it by the
	# number of fireballs that we want to shoot so that they're evenly spaced
	# out.
	var angle:float = PI * 2 / num_fireballs

	for i in range(num_fireballs):
		var vector:Vector2 = Vector2.UP.rotated(i * angle).normalized()

		var projectile_params:ProjectileParams = ProjectileParams.new({
			"firer": player,
			"override_start_position": player.get_mouse_position(),
			"projectile_type": projectile_type,
			"damage": player.stats.attack,
			"target": vector,
			"treat_target_as_position": false,
		})
		player.fire_projectile(projectile_params)

	queue_free()
