extends Node2D

class_name FrozenOrbAbility

var player:PlayerController
const energy_cost:int = 35

static func get_description(_player_with_ability) -> String:
	return "Frozen Orb - fires an orb that shoots icebolts in all directions. Costs %d energy." % [energy_cost]

func _init(player_node:PlayerController) -> void:
	player = player_node

# This ability is only concerned with shooting a special kind of projectile.
# That projectile has its own script to shoot other projectiles.
func use_ability() -> void:
	var mouse_position:Vector2 = player.get_mouse_position()
	
	var projectile_params:ProjectileParams = ProjectileParams.new({
		"firer": player,
		"projectile_type": Constants.ProjectileTypes.FROZEN_ORB,
		"damage": player.stats.attack,
		"target": mouse_position,
		"treat_target_as_position": true,
	})
	player.fire_projectile(projectile_params)
	
	queue_free()
