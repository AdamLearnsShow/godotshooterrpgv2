extends Node2D

class_name RallyingCryAbility

var RallyingCryParticlesScene:PackedScene = preload("res://scenes/particles/RallyingCryParticles.tscn")

var player:PlayerController
var timer:Timer = null
const energy_cost:int = 30
var buff_properties:Dictionary

# This indicates whether this particular instance of the buff was the one to
# modify the health-regen stat.
var modified_health_regen:bool = false

# Buffs were coded to show off the system and add some variety to abilities, but
# due to how they were coded, the Stats object is modified directly for
# health_regen, so this number can never change without re-coding how buffs are
# applied.
const health_regen_buff:float = 4.0

static func get_description(player_with_ability:PlayerController) -> String:
	var ability_params:Dictionary = get_ability_parameters(player_with_ability.stats.level)

	var duration:float = ability_params.duration
	var heal_amount:float = ability_params.heal_amount
	var defense_buff:float = ability_params.defense_buff
	return "Rallying Cry - immediately heal for %d, then increase health regen by %.1f and defense by %.1f for %.1f seconds. Costs %d energy." % [heal_amount, health_regen_buff, defense_buff, duration, energy_cost]

static func get_ability_parameters(player_level:int) -> Dictionary:
	return {
		"duration": 5.0 + (player_level - 1) * 0.2,
		"heal_amount": 25 + player_level * 5,
		"health_regen_buff": 4.0,
		"defense_buff": 2.0 + (player_level - 1) * 0.3
	}
	
func _init(player_node:PlayerController) -> void:
	player = player_node
	
	var ability_params:Dictionary = get_ability_parameters(player.stats.level)

	var duration:float = ability_params.duration
	var defense_buff:float = ability_params.defense_buff
	
	buff_properties = {
		"name": Constants.BuffNames.RALLYING_CRY,
		"duration": duration,
		"props_to_inject": {
			"defense_buff": defense_buff
		}
	}

func use_ability() -> void:
	var ability_params:Dictionary = get_ability_parameters(player.stats.level)
	var heal_amount:float = ability_params.heal_amount
	
	player.stats.restore_life(heal_amount)

	var buff_output:Dictionary = player.add_buff(self)
	timer = buff_output.timer

	if !buff_output.did_extend_existing_buff:
		modified_health_regen = true
		player.stats.health_regen += health_regen_buff

		var particles:Node = RallyingCryParticlesScene.instance()
		player.add_child(particles)

	var _ignored = timer.connect("timeout", self, "_on_timeout")

func _on_timeout() -> void:
	var _ignored = player.buffs.erase(buff_properties.name)

	# If that was the last instance of the buff, then remove the particles
	if !player.has_buff(Constants.BuffNames.RALLYING_CRY):
		player.remove_particles("RallyingCryParticles")

	if modified_health_regen:
		player.stats.health_regen -= health_regen_buff

	queue_free()
