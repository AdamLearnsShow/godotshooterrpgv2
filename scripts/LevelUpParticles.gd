extends CPUParticles2D

func _ready() -> void:
	emitting = true
	var _ignored = $CleanUpTimer.connect("timeout", self, "queue_free")
