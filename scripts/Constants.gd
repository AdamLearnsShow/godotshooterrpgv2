extends Node

enum CharacterClassNames { ARCHER, WARRIOR, MAGE }
enum EnemyNames { RAT, BAT, SNAKE, BEHOLDER, SPIDER, SPECTER, SKELETON, IMP, FLOATING_FIRE, GOLD_DRAGON, BLACK_DRAGON, GOBLIN }
enum BuffNames { RAPID_FIRE, RALLYING_CRY }
enum AbilityNames { MULTISHOT, RAPID_FIRE, FROZEN_ORB, RALLYING_CRY, FIRE_EXPLOSION }
enum PhysicsMasks { WORLD_WALLS, WORLD_PITS, PLAYER, ENEMY, PLAYER_PROJECTILE, ENEMY_PROJECTILE }
enum ProjectileTypes { ARROW, AXE, FIREBALL, FROZEN_ORB, ICEBOLT, DART, JAVELIN, METEOR, NEEDLE, ROCK }
enum AIBehavior {
	# The AI is doing nothing.
	IDLE,

	# The AI finds a target, stores its position, and then pursues that. This
	# means that if the target moves, the AI won't necessarily update immediately.
	PURSUE_POINT,

	# The AI finds a target and stores the target itself, meaning it will home
	# in on the target.
	PURSUE_TARGET,

	# The AI finds a target and tries to run away from that target.
	EVADE_TARGET,

	# The AI finds a target within a certain number of tiles and goes there
	PURSUE_RANDOM_POINT,
}
const GroupNameBillboarded := "billboarded"
const GroupNameMonsterZones := "monster zones"
const GroupNameEnemies := "enemies"
const GroupNameProjectiles := "projectiles"
const GroupNamePlayers := "players"
const tilesize := 32

var ability_info := {
	AbilityNames.MULTISHOT: {
		"script": MultishotAbility,
	},
	AbilityNames.RAPID_FIRE: {
		"script": RapidFireAbility,
	},
	AbilityNames.FROZEN_ORB: {
		"script": FrozenOrbAbility,
	},
	AbilityNames.FIRE_EXPLOSION: {
		"script": FireExplosionAbility,
	},
	AbilityNames.RALLYING_CRY: {
		"script": RallyingCryAbility,
	}
}

var character_class_info := {
	CharacterClassNames.ARCHER: {
		"scene_path": "res://scenes/entities/Archer.tscn",
		"projectile_type": ProjectileTypes.ARROW,
		"ability_unlocks": [
			{
				"level": 1,
				"name": AbilityNames.MULTISHOT,
			},
			{
				"level": 1,
				"name": AbilityNames.RAPID_FIRE,
			}
		],
		"starting_stats": {
			"max_life": 100,
			"max_energy": 100,
			"health_regen": 0.1,
			"energy_regen": 4,
			"attack": 8,
			"fire_reset": 0.42,
			"defense": 1,
			"movement_speed": 300
		},
		"stat_growth": {
			"max_life": 10,
			"max_energy": 10,
			"health_regen": 0.1,
			"energy_regen": 1,
			"fire_reset": 0.01,
			"attack": 1,
			"defense": 0.2,
			"movement_speed": 5
		},
	},
	CharacterClassNames.WARRIOR: {
		"scene_path": "res://scenes/entities/Warrior.tscn",
		"projectile_type": ProjectileTypes.AXE,
		"ability_unlocks": [
			{
				"level": 1,
				"name": AbilityNames.RALLYING_CRY,
			},
			{
				"level": 1,
				"name": AbilityNames.RAPID_FIRE,
			}
		],
		"starting_stats": {
			"max_life": 100,
			"max_energy": 100,
			"health_regen": 0.2,
			"energy_regen": 2,
			"attack": 17,
			"fire_reset": 0.8,
			"defense": 2,
			"movement_speed": 200
		},
		"stat_growth": {
			"max_life": 10,
			"max_energy": 10,
			"health_regen": 0.1,
			"energy_regen": 0.7,
			"attack": 1,
			"fire_reset": 0.01,
			"defense": 0.2,
			"movement_speed": 5
		},
	},
	CharacterClassNames.MAGE: {
		"scene_path": "res://scenes/entities/Mage.tscn",
		"projectile_type": ProjectileTypes.FIREBALL,
		"ability_unlocks": [
			{
				"level": 1,
				"name": AbilityNames.FROZEN_ORB,
			},
			{
				"level": 1,
				"name": AbilityNames.FIRE_EXPLOSION,
			}
		],
		"starting_stats": {
			"max_life": 80,
			"max_energy": 100,
			"health_regen": 0.1,
			"energy_regen": 5,
			"attack": 12,
			"fire_reset": 0.6,
			"defense": 0,
			"movement_speed": 250
		},
		"stat_growth": {
			"max_life": 10,
			"max_energy": 10,
			"health_regen": 0.1,
			"energy_regen": 1,
			"attack": 1,
			"fire_reset": 0.01,
			"defense": 0.2,
			"movement_speed": 5
		},
	}
}

var projectile_info := {
	ProjectileTypes.ARROW: {
		"scene_path": "res://scenes/projectiles/Arrow.tscn",
		"ttl": 2,
		"speed": 440
	},
	ProjectileTypes.AXE: {
		"scene_path": "res://scenes/projectiles/Axe.tscn",
		"rotation_speed": 7.0,
		"ttl": 0.75,
		"speed": 160
	},
	ProjectileTypes.FIREBALL: {
		"scene_path": "res://scenes/projectiles/Fireball.tscn",
		"is_billboarded": true,
		"ttl": 1,
		"speed": 400
	},
	ProjectileTypes.FROZEN_ORB: {
		"scene_path": "res://scenes/projectiles/Icebolt.tscn",
		"script_path": "res://scripts/projectiles/FrozenOrb.gd",
		"ttl": 2,
		"speed": 100
	},
	ProjectileTypes.ICEBOLT: {
		"scene_path": "res://scenes/projectiles/Icebolt.tscn",
		"ttl": 3,
		"speed": 160
	},
	ProjectileTypes.DART: {
		"scene_path": "res://scenes/projectiles/Dart.tscn",
		"ttl": 1,
		"speed": 400
	},
	ProjectileTypes.JAVELIN: {
		"scene_path": "res://scenes/projectiles/Javelin.tscn",
		"ttl": 3,
		"speed": 160
	},
	ProjectileTypes.METEOR: {
		"scene_path": "res://scenes/projectiles/Meteor.tscn",
		"ttl": 13,
		"speed": 80
	},
	ProjectileTypes.NEEDLE: {
		"scene_path": "res://scenes/projectiles/Needle.tscn",
		"ttl": 1.2,
		"speed": 380
	},
	ProjectileTypes.ROCK: {
		"scene_path": "res://scenes/projectiles/Rock.tscn",
		"ttl": 1.5,
		"speed": 200
	}
}

const experience_needed_to_level:Array = [
	100,
	150,
	250,
	300,
	350,
	400,
	450,
	500,
	550,
	600,
	700,
	750,
	800,
	900,
	1000,
	1250,
	1500,
	1500,
	1500
]

# For simplicity's sake, this will be the default set of stats for enemies.
const enemy_stats_easy := {
	"max_life": 20,
	"experience": 10,
	"attack": 10,
	"defense": 2,
	"movement_speed": 100,
	"fire_reset": 1.0,
}

# These are some simple defaults for an enemy that grows only a little bit in
# power every level.
const enemy_stat_growth_easy := {
	"max_life": 10,
	"experience": 5,
	"attack": 1,
	"defense": 0.2,
	"movement_speed": 1,
	"fire_reset": 0.01,
}

const enemy_stats_medium := {
	"max_life": 80,
	"experience": 30,
	"attack": 14,
	"defense": 2,
	"movement_speed": 90,
	"fire_reset": 1.0,
}

# These are some simple defaults for an enemy that grows only a little bit in
# power every level.
const enemy_stat_growth_medium := {
	"max_life": 20,
	"experience": 10,
	"attack": 2,
	"defense": 0.4,
	"movement_speed": 3,
	"fire_reset": 0.01,
}

var enemy_info := {
	EnemyNames.RAT: {
		"scene_path": "res://scenes/entities/Rat.tscn",
		"projectile_type": ProjectileTypes.DART,
		"starting_stats": enemy_stats_easy,
		"stat_growth": enemy_stat_growth_easy,
	},
	EnemyNames.BAT: {
		"scene_path": "res://scenes/entities/Bat.tscn",
		"projectile_type": ProjectileTypes.NEEDLE,
		"starting_stats": enemy_stats_easy,
		"stat_growth": enemy_stat_growth_easy,
	},
	EnemyNames.SNAKE: {
		"scene_path": "res://scenes/entities/Snake.tscn",
		"scale": 1.3,
		"projectile_type": ProjectileTypes.ROCK,
		"starting_stats": enemy_stats_medium,
		"stat_growth": enemy_stat_growth_medium,
	},
	EnemyNames.BEHOLDER: {
		"scene_path": "res://scenes/entities/Beholder.tscn",
		"scale": 1.6,
		"projectile_type": ProjectileTypes.FIREBALL,
		"starting_stats": enemy_stats_medium,
		"stat_growth": enemy_stat_growth_medium,
	},
	EnemyNames.SPIDER: {
		"scene_path": "res://scenes/entities/Spider.tscn",
		"scale": 0.6,
		"projectile_type": ProjectileTypes.DART,
		"starting_stats": {
			"max_life": 1,
			"experience": 7,
			"attack": 10,
			"defense": 0,
			"movement_speed": 100,
			"fire_reset": 1.0,
		},
		"stat_growth": enemy_stat_growth_easy,
	},
	EnemyNames.SPECTER: {
		"scene_path": "res://scenes/entities/Specter.tscn",
		"projectile_type": ProjectileTypes.JAVELIN,
		"starting_stats": enemy_stats_easy,
		"stat_growth": enemy_stat_growth_easy,
	},
	EnemyNames.SKELETON: {
		"scene_path": "res://scenes/entities/Skeleton.tscn",
		"projectile_type": ProjectileTypes.ARROW,
		"starting_stats": enemy_stats_easy,
		"stat_growth": enemy_stat_growth_easy,
	},
	EnemyNames.IMP: {
		"scene_path": "res://scenes/entities/Imp.tscn",
		"scale": 1.6,
		"starting_stats": {
			"max_life": 30,
			"experience": 20,
			"attack": 11,
			"defense": 1,
			"movement_speed": 170,
			"fire_reset": 1.3,
		},
		"projectile_type": ProjectileTypes.FIREBALL,
		"stat_growth": enemy_stat_growth_easy,
	},
	EnemyNames.FLOATING_FIRE: {
		"scene_path": "res://scenes/entities/FloatingFire.tscn",
		"scale": 0.9,
		"projectile_type": ProjectileTypes.FIREBALL,
		"starting_stats": enemy_stats_easy,
		"stat_growth": enemy_stat_growth_easy,
	},
	EnemyNames.GOLD_DRAGON: {
		"scene_path": "res://scenes/entities/GoldDragon.tscn",
		"scale": 3,
		"projectile_type": ProjectileTypes.FIREBALL,
		"starting_stats": {
			"max_life": 700,
			"experience": 150,
			"attack": 20,
			"defense": 1,
			"movement_speed": 170,
			"fire_reset": 1.0,
		},
		"stat_growth": {
			"max_life": 40,
			"experience": 5,
			"attack": 1,
			"defense": 0.2,
			"movement_speed": 1,
			"fire_reset": 0.01,
		},
	},
	EnemyNames.BLACK_DRAGON: {
		"scene_path": "res://scenes/entities/BlackDragon.tscn",
		"scale": 4,
		"projectile_type": ProjectileTypes.METEOR,
		"starting_stats": {
			"max_life": 800,
			"experience": 150,
			"attack": 50,
			"defense": 4,
			"movement_speed": 50,
			"fire_reset": 1.0,
		},
		"stat_growth": {
			"max_life": 50,
			"experience": 5,
			"attack": 3,
			"defense": 0.2,
			"movement_speed": 1,
			"fire_reset": 0.01,
		},
	},
	EnemyNames.GOBLIN: {
		"scene_path": "res://scenes/entities/Goblin.tscn",
		"projectile_type": ProjectileTypes.AXE,
		"starting_stats": enemy_stats_easy,
		"stat_growth": enemy_stat_growth_easy,
	},
}
