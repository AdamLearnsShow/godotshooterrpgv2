extends KinematicBody2D

class_name PlayerController

signal rotated(rotation)
signal fired(projectile_params)

# A vector formed from the player's input to represent where the player wants to
# move.
var velocity:Vector2
var character_class_info:Dictionary
var camera:Camera2D

# Either -1, 0, or 1 based on the player's input to indicate which direction
# they want to rotate the camera.
var camera_rotate_delta:int
const camera_rotate_speed:float = 2.0
const camera_distance:int = 70

# When this hits 0, the player is able to fire again.
var fire_cooldown:float = 0

# The time, in seconds, for how long the player takes before they can fire again
var fire_reset:float = 0.42
var should_fire:bool = false
var stats:Stats

# The buff system is intended to be relatively light (as opposed to a generic
# system that could handle practically anything).
var buffs:Dictionary = {}

# This is an array of ability enum values, e.g. Constants.AbilityNames.MULTISHOT
var unlocked_active_abilities:Array

func _ready() -> void:
	collision_layer = Util.convert_enum_to_bitmask([Constants.PhysicsMasks.PLAYER])
	collision_mask = Util.convert_enum_to_bitmask([
		Constants.PhysicsMasks.WORLD_WALLS,
		Constants.PhysicsMasks.WORLD_PITS,
		Constants.PhysicsMasks.ENEMY,
		Constants.PhysicsMasks.ENEMY_PROJECTILE
	])
	
	z_index = 1
	
	add_to_group(Constants.GroupNamePlayers)
	stats.set_up_regen_timer(self)

func init(character_class:int) -> void:
	character_class_info = Constants.character_class_info[character_class]

	position.x = 50
	position.y = 50
	name = "Player"
	
	setup_camera()
	setup_stats()
	potentially_unlock_abilities()

func setup_camera() -> void:
	camera = Camera2D.new()
	camera.name = "Camera2D"
	camera.zoom = Vector2(1/1.5, 1/1.5)
	camera.current = true
	camera.rotating = true
	camera.position.y = -camera_distance
	add_child(camera)

func setup_stats() -> void:
	var starting_stats:Dictionary = character_class_info.starting_stats
	var stat_growth:Dictionary = character_class_info.stat_growth
	
	stats = Stats.new(1, starting_stats, stat_growth)
	add_child(stats)
	
	var _ignored = stats.connect("leveled_up", self, "_on_leveled_up")
	
func _on_leveled_up(_level:int) -> void:
	emit_level_up_particles()
		
	Util.add_sound_to_node_by_sound_file("res://assets/sounds/level_up.wav", self)
	potentially_unlock_abilities()

func emit_level_up_particles() -> void:	
	var LevelUpParticles:PackedScene = preload("res://scenes/particles/LevelUpParticles.tscn")
	var particles:CPUParticles2D = LevelUpParticles.instance()
	add_child(particles)

func use_ability(ability_index:int) -> void:
	if ability_index < 0 || ability_index >= unlocked_active_abilities.size():
		return

	var ability_to_use:int = unlocked_active_abilities[ability_index]
	var ability_script:GDScript = Constants.ability_info[ability_to_use].script

	var ability_instance:Node = ability_script.new(self)
	var energy_cost:float = ability_instance.energy_cost
	if !stats.has_enough_energy(energy_cost):
		ability_instance.queue_free()
		return

	stats.remove_energy(energy_cost)

	add_child(ability_instance)
	ability_instance.use_ability()

# This should be called at construction time and every time the player levels up
# so that we can unlock any level-based abilities they have.
func potentially_unlock_abilities() -> void:
	var ability_unlocks:Array = character_class_info.ability_unlocks

	for ability_unlock_info in ability_unlocks:
		var unlock_level:int = ability_unlock_info.level
		var ability_name:int = ability_unlock_info.name

		if stats.level >= unlock_level and unlocked_active_abilities.find(ability_name) == -1:
			unlocked_active_abilities.append(ability_name)

func fire_projectile(projectile_params:ProjectileParams) -> void:
	emit_signal("fired", projectile_params)
	
func _unhandled_input(_event:InputEvent) -> void:
	# Reset our velocity back to nothing
	velocity = Vector2()
	camera_rotate_delta = 0
	should_fire = false
	
	if Input.is_action_pressed("fire"):
		should_fire = true
				
	if Input.is_action_pressed("rotate_right"):
		camera_rotate_delta = 1
	if Input.is_action_pressed("rotate_left"):
		camera_rotate_delta = -1
		
	if Input.is_action_pressed("ui_right"):
		velocity.x += 1
	if Input.is_action_pressed("ui_left"):
		velocity.x -= 1
	if Input.is_action_pressed("ui_down"):
		velocity.y += 1
	if Input.is_action_pressed("ui_up"):
		velocity.y -= 1
	if velocity.length() > 0:
		velocity = velocity.normalized() * stats.movement_speed
		
	if Input.is_action_just_pressed("digit_one"):
		use_ability(0)
	if Input.is_action_just_pressed("digit_two"):
		use_ability(1)
	if Input.is_action_just_pressed("digit_three"):
		use_ability(2)

func _physics_process(delta:float) -> void:
	rotation += camera_rotate_delta * camera_rotate_speed * delta
	fire_cooldown -= delta
	
	# Delta is already calculated in move_and_slide
	var _ignored = self.move_and_slide(velocity.rotated(rotation))
	
	# If we collided with a projectile, run the hit function as though it
	# collided with us.
	if get_slide_count() > 0:
		var collision:KinematicCollision2D = get_slide_collision(0)
		var body:Object = collision.collider
		if body.is_in_group(Constants.GroupNameProjectiles) && body.has_method("collided_with_body"):
			body.collided_with_body(self)

	if camera_rotate_delta != 0:
		emit_signal("rotated", rotation)
	
	if should_fire:
		fire_main_projectile()

# Thanks for this hack, Neui! This is to work around this bug:
# https://github.com/godotengine/godot/issues/32222
func get_mouse_position() -> Vector2:
	return get_canvas_transform().affine_inverse().xform(get_viewport().get_parent().get_global_mouse_position())
	
func fire_main_projectile() -> void:
	if fire_cooldown > 0:
		return
		
	fire_cooldown = get_fire_reset()
	
	var projectile_params:ProjectileParams = ProjectileParams.new({
		"firer": self,
		"projectile_type": character_class_info.projectile_type,
		"damage": stats.attack,
		"target": get_mouse_position(),
		"treat_target_as_position": true,
	})
	Util.add_sound_to_node_by_sound_file("res://assets/sounds/shoot.wav", self)
	fire_projectile(projectile_params)

# Gets the player's defense, taking their buffs into account.
func get_total_defense() -> float:
	var base_defense:float = stats.defense
	
	var bonus_defense:float = 0

	if has_buff(Constants.BuffNames.RALLYING_CRY):
		bonus_defense += buffs[Constants.BuffNames.RALLYING_CRY].defense_buff
	
	return base_defense + bonus_defense

# Gets the player's fire_reset, taking their buffs into account.
func get_fire_reset() -> float:
	var multiplier:float = 1
	
	if has_buff(Constants.BuffNames.RAPID_FIRE):
		multiplier = buffs[Constants.BuffNames.RAPID_FIRE].reset_multiplier

	return stats.fire_reset * multiplier

func projectile_hit(projectile:Projectile) -> void:
	Util.add_sound_to_node_by_sound_file("res://assets/sounds/player_hit.wav", self)
	var defense:float = get_total_defense()

	stats.remove_life(Stats.get_damage_from_attack_and_defense(projectile.get_damage(), defense))

func has_buff(buff_name:int) -> bool:
	return buff_name in buffs

func extend_buff_timer(buff_name:int, buff_duration:float) -> bool:
	if has_buff(buff_name):
		var timer:Timer = buffs[buff_name].timer
		timer.start(timer.wait_time + buff_duration)
		return true

	return false

# The buff itself will become the owner for the timer, that way, when it frees
# itself, its child timer also gets freed.
func add_buff(buff:Node):
	Util.add_sound_to_node_by_sound_file("res://assets/sounds/gain_buff.wav", self)
	var buff_properties:Dictionary = buff.buff_properties
	var buff_name:int = buff_properties.name
	var buff_duration:float = buff_properties.duration
	var props_to_inject:Dictionary = buff_properties.props_to_inject

	var did_extend_existing_buff:bool = false
	var timer:Timer

	if has_buff(buff_name):
		timer = buffs[buff_name].timer
		timer.start(timer.wait_time + buff_duration)
		did_extend_existing_buff = true
	else:
		timer = Timer.new()
		buff.add_child(timer)
		timer.wait_time = buff_duration
		timer.start()

		buffs[buff_name] = {
			"timer": timer
		}

	# Regardless of whether we extended the buff, update its injected properties.
	for key in props_to_inject:
		buffs[buff_name][key] = props_to_inject[key]

	return {
		"did_extend_existing_buff": did_extend_existing_buff,
		"timer": timer
	}

func remove_particles(particle_scene_name:String) -> void:
	if !has_node(particle_scene_name):
		return
		
	var node:Node = get_node(particle_scene_name)
	remove_child(node)
	node.queue_free()
