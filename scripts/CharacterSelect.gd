extends Control

signal selected_character(character_class)

func _unhandled_input(_event:InputEvent) -> void:
	if Input.is_action_just_pressed("digit_one"):
		emit_signal("selected_character", Constants.CharacterClassNames.ARCHER)
	if Input.is_action_just_pressed("digit_two"):
		emit_signal("selected_character", Constants.CharacterClassNames.WARRIOR)
	if Input.is_action_just_pressed("digit_three"):
		emit_signal("selected_character", Constants.CharacterClassNames.MAGE)
