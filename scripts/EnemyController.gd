extends KinematicBody2D

signal died(experience)
signal fired(projectile_params)

# This represents the current AI behavior.
var current_behavior:int

# This is when we last set current_behavior so that we can bail out of
# particular AI routines if we've been in them for too long.
var time_of_setting_behavior:int

# These are parameters specific to each individual AI routine.
var ai_params:Dictionary = {}

# When pursuing points, we don't necessarily want to go directly to that point.
var fuzz_amount_when_pursuing_point:int = 96

# This is just one of the enemy_info dictionaries from Constants.gd
var enemy_info:Dictionary

# How far away the player has to be in pixels before this enemy detects them
var aggro_radius:int = 18 * Constants.tilesize

# This prevents us from granting experience multiple times if multiple
# projectiles collide with an enemy on the same physics tick.
var is_dead:bool = false

var stats:Stats

func _ready() -> void:
	set_current_behavior(Constants.AIBehavior.IDLE)
	set_up_ai_timer()
	set_up_fire_timer()
	stats.set_up_regen_timer(self)

func init(starting_rotation:float, enemy_info_dict:Dictionary, level:int) -> void:
	enemy_info = enemy_info_dict
	
	if "scale" in enemy_info:
		scale = Vector2(enemy_info.scale, enemy_info.scale)

	add_to_group(Constants.GroupNameEnemies)
	add_to_group(Constants.GroupNameBillboarded)
	rotation = starting_rotation;
	
	stats = Stats.new(level, enemy_info.starting_stats, enemy_info.stat_growth)

	collision_layer = Util.convert_enum_to_bitmask([Constants.PhysicsMasks.ENEMY])
	collision_mask = Util.convert_enum_to_bitmask([Constants.PhysicsMasks.PLAYER_PROJECTILE, Constants.PhysicsMasks.PLAYER])

func set_up_fire_timer() -> void:
	var timer:Timer = Timer.new()
	add_child(timer)
	timer.wait_time = randf()

	while true:
		fire_projectile()
		timer.start()
		yield(timer, "timeout")
		timer.wait_time = stats.fire_reset

# Sets up the timer that's going to handle changing AI.
func set_up_ai_timer() -> void:
	var timer:Timer = Timer.new()
	add_child(timer)
	timer.wait_time = 2

	while true:
		ai_tick()
		timer.start()
		yield(timer, "timeout")

# This is called every so often to try to change AI.
func ai_tick() -> void:
	var elapsed_time_with_current_ai:int = OS.get_ticks_msec() - time_of_setting_behavior
	if elapsed_time_with_current_ai > 2000:
		set_current_behavior(Constants.AIBehavior.IDLE)

	if current_behavior == Constants.AIBehavior.IDLE:
		var num_behaviors:int = 4
		var rand_behavior:int = randi() % num_behaviors
		
		# Two of our behaviors should show up less frequently, so if we roll
		# those numbers, then we'll reroll them.
		if rand_behavior == 2 or rand_behavior == 3:
			rand_behavior = randi() % num_behaviors

		match rand_behavior:
			0: find_player_to_pursue_as_target()
			1: find_player_to_pursue_as_point()
			2: find_player_to_evade()
			3: find_random_point_to_pursue()

# This locates any target within the "players" group
func find_player_generic() -> Node2D:
	var players:Array = get_tree().get_nodes_in_group(Constants.GroupNamePlayers)

	if players.empty():
		return null

	var target:Node2D = players[0]

	# Make sure this enemy can see the player
	if global_position.distance_to(target.global_position) > aggro_radius:
		return null

	return players[0]

# This is for the PURSUE_POINT AI.
func find_player_to_pursue_as_point() -> void:
	var target:Node2D = find_player_generic()
	if target == null:
		return

	set_current_behavior(Constants.AIBehavior.PURSUE_POINT)

	var target_position:Vector2 = target.position

	var distance_to_target:float = target_position.distance_to(position)

	# Change the fuzz based on their distance from you
	var fuzz_multiplier:float = min(distance_to_target / 320.0, 3)
	var fuzz:float = fuzz_amount_when_pursuing_point * fuzz_multiplier

	target_position.x += rand_range(-fuzz, fuzz)
	target_position.y += rand_range(-fuzz, fuzz)

	ai_params = {
		"position": target_position
	}

func find_random_point_to_pursue() -> void:
	set_current_behavior(Constants.AIBehavior.PURSUE_RANDOM_POINT)

	var min_distance:int = 5 * Constants.tilesize
	var max_distance:int = 14 * Constants.tilesize
	var target_position:Vector2 = global_position + Util.rand_vector2() * rand_range(min_distance, max_distance)

	ai_params = {
		"position": target_position
	}

# This is for the PURSUE_TARGET AI.
func find_player_to_pursue_as_target() -> void:
	var target:Node2D = find_player_generic()
	if target == null:
		return

	set_current_behavior(Constants.AIBehavior.PURSUE_TARGET)

	ai_params = {
		"target": target
	}

func find_player_to_evade() -> void:
	var target:Node2D = find_player_generic()
	if target == null:
		return

	set_current_behavior(Constants.AIBehavior.EVADE_TARGET)

	ai_params = {
		"target": target
	}

# Sets the current AI behavior and the time when we set this behavior.
func set_current_behavior(new_behavior:int) -> void:
	time_of_setting_behavior = OS.get_ticks_msec()
	current_behavior = new_behavior

func _physics_process(delta:float) -> void:
	if current_behavior == Constants.AIBehavior.PURSUE_POINT or current_behavior == Constants.AIBehavior.PURSUE_RANDOM_POINT:
		ai_pursue_point(delta)
	elif current_behavior == Constants.AIBehavior.PURSUE_TARGET:
		ai_pursue_target(delta)
	elif current_behavior == Constants.AIBehavior.EVADE_TARGET:
		ai_evade_target(delta)

# This is the logic of PURSUE_TARGET.
func ai_pursue_target(delta:float) -> void:
	var target_position:Vector2 = ai_params.target.position

	ai_pursue_position(target_position, delta)

func ai_evade_target(delta:float) -> void:
	# Draw a vector from the target to the enemy, extend it, and pursue that
	# position
	var target_position:Vector2 = ai_params.target.global_position
	var vector:Vector2 = target_position.direction_to(global_position)
	vector = global_position + vector * 1000

	ai_pursue_position(vector, delta)

# This is the logic of PURSUE_POINT.
func ai_pursue_point(delta:float) -> void:
	var target_position = ai_params.position
	ai_pursue_position(target_position, delta)

# This is a generic function that can be called to directly move toward a
# particular point.
func ai_pursue_position(target_position:Vector2, delta:float) -> void:
	var movement_amount:float = stats.movement_speed * delta
	var vector:Vector2 = position.direction_to(target_position) * movement_amount
	position += vector

	if position.distance_to(target_position) <= movement_amount:
		set_current_behavior(Constants.AIBehavior.IDLE)

func fire_projectile() -> void:
	var target:Node2D = find_player_generic()
	if target == null:
		return
	var target_vector:Vector2 = global_position.direction_to(target.global_position)

	var fuzz_amount:float = PI / 6
	target_vector = target_vector.rotated(rand_range(-fuzz_amount, fuzz_amount))

	var projectile_params:ProjectileParams = ProjectileParams.new({
		"firer": self,
		"projectile_type": enemy_info.projectile_type,
		"scale": scale.x,
		"damage": stats.attack,
		"target": target_vector,
		"treat_target_as_position": false,
	})
	emit_signal("fired", projectile_params)

func projectile_hit(projectile:Projectile) -> void:
	Util.add_sound_to_node_by_sound_file("res://assets/sounds/hit.wav", get_parent())
	stats.remove_life(Stats.get_damage_from_attack_and_defense(projectile.get_damage(), stats.defense))
	if stats.life <= 0 and !is_dead:
		is_dead = true
		emit_signal("died", stats.experience)
		queue_free()
