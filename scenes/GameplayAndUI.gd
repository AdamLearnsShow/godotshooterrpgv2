extends Node2D

onready var viewport_container:ViewportContainer = $ViewportContainer
onready var gameplay:Gameplay = $ViewportContainer/Viewport/Gameplay
onready var ui:GameplayUI = $UI

func _ready() -> void:
	var window_width:int = ProjectSettings.get_setting("display/window/size/width")
	var window_height:int = ProjectSettings.get_setting("display/window/size/height")
	var ui_width:int = 200
	
	viewport_container.margin_right = window_width - ui_width
	viewport_container.margin_bottom = window_height
	
	ui.offset.x = window_width - ui_width

func init(character_class:int) -> void:
	gameplay.init(character_class, ui)
